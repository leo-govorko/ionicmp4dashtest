import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import dashjs from 'dashjs';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage 
{

  constructor(public navCtrl: NavController) 
  {	
  }
  
  ionViewDidLoad() 
  {  	
	var url = "http://dash.edgesuite.net/akamai/test/caption_test/ElephantsDream/elephants_dream_480p_heaac5_1.mpd";
	var player = dashjs.MediaPlayer().create();	
	player.initialize(document.getElementById('player'), url, true);		
	player.attachTTMLRenderingDiv(document.getElementById('subtitles') as HTMLDivElement);
	player.displayCaptionsOnTop(true);
  }  
  
}
